<%@ page import="com.loanapp.loanmanagmentproject.appPack.loanManaging.Loan" %>
<%@ page import="java.util.List" %>
<%@ page import="com.loanapp.loanmanagmentproject.utilities.UrlObject" %><%--
  Created by IntelliJ IDEA.
  User: LoicRaveloarimanana
  Date: 25/09/2023
  Time: 12:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    List<Loan> loanList = (List<Loan>) request.getAttribute("data");
    String pagetTitle = (String) request.getAttribute("title");
    UrlObject urlObject = (UrlObject) request.getAttribute("url");
    int pagesTotal = (int) request.getAttribute("nPage");

%>
<html>
<head>
    <title><%=pagetTitle%></title>
</head>
<body>
<table border="1">
    <tr>
        <th>code</th>
        <th>account number</th>
        <th>date</th>
        <th>taux</th>
        <th>montant</th>
        <th>duration</th>
        <th>interet</th>
        <th>totale</th>
    </tr>
    <% for(Loan loan : loanList) { %>
    <tr>
        <td><%=loan.getCode()%></td>
        <td><%=loan.getBeneficiaireAccount().getAccountNumber()%></td>
        <td><%=loan.getDate()%></td>
        <td><%=loan.getTauxInteret()%></td>
        <td><%=loan.getAmount()%></td>
        <td><%=loan.getDuration()%></td>
        <td><%=loan.getIneteret()%></td>
        <td><%=loan.getTotaleValue()%></td>
    </tr>
   <% } %>

</table>
<% if(request.getAttribute("error") != null )  { %>
<p style="color: red"><%=request.getAttribute("error")%></p>
<% } %>
</body>
</html>
