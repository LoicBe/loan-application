<%--
  Created by IntelliJ IDEA.
  User: LoicRaveloarimanana
  Date: 23/09/2023
  Time: 21:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>new loan </title>
</head>
<body>
<form action="<%=request.getContextPath()%>/loanApp/loanApi/createLoan" method="post">
  <label> account
    <input type="text" name="accountNumber">
  </label>
  <label> amount
    <input type="text" name="amount">
  </label>
  <label> duration
    <input type="number" name="duration">
  </label>
  <label> date
    <input type="datetime-local" name="date">
  </label>
  <input type="submit" value="ok">
</form>
<% if (request.getAttribute("error") != null)   { %>
<p><%=request.getAttribute("error")%></p>
<%  }   %>
</body>
</html>
