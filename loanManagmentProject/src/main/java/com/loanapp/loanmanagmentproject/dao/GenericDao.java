package com.loanapp.loanmanagmentproject.dao;

import com.loanapp.loanmanagmentproject.utilities.PaginationHelper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

@Repository
public class GenericDao {


    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public <T> T create(T entity) throws Exception{
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
        session.close();
        return entity;
    }

    public <T> T findById(Class<T> clazz, Serializable id) {
        Session session = sessionFactory.openSession();
        T entity = (T) session.get(clazz, id);
        session.close();
        return entity;
    }


    public <T> List<T> findAll(Class<T> tClass){
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(tClass).list();
        session.close();
        return results;
    }

    public <T> List<T> findWhere(T entity){
        Session session = sessionFactory.openSession();
        Example example = Example.create(entity).ignoreCase();
        List<T> results = session.createCriteria(entity.getClass()).add(example).list();
        session.close();
        return results;
    }

    public void delete(Object entity){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(entity);
        transaction.commit();
        session.close();
    }

    public <T> T update(T entity){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
        session.close();
        return entity;
    }


    //count sql required
    public int countRow(String sql , Session session) throws Exception {
        BigInteger row  = (BigInteger) session.createSQLQuery(sql).uniqueResult();
        return row.intValue();
    }


    //unique result query
    public <T> T fecthUniqueResult(Class<T> clazz , String sql) throws Exception {
        Session session = sessionFactory.openSession();
        System.out.println("tonga ato ");
        T val  =(T) session.createSQLQuery(sql).addEntity(clazz).uniqueResult();
        session.close();
        return  val;
    }



    public HashMap pagination1(Class<?> clazz , String queryResultCount , String query , int numPage , int limit) throws Exception {
        Session session = sessionFactory.openSession();
        HashMap result =  new HashMap<>();
        int rowCount = countRow(queryResultCount , session);
        int offset = (numPage-1)*limit;
        int pages = PaginationHelper.getTotalePages(rowCount,limit);
        result.put("nbrePage" , pages);
        result.put("data" , session.createSQLQuery(query+" LIMIT "+limit+" OFFSET "+offset).addEntity(clazz).list());
        session.close();
        return result;
    }

    public HashMap pagination(Class<?> clazz ,String tableName , int numPage , int limit) throws Exception {
        String queryResultCount = "SELECT count(*) FROM "+tableName;
        String query = "SELECT * FROM "+tableName;
        return pagination1(clazz,queryResultCount,query,numPage,limit);
    }

    public <T> List<T> fecthResult(String sql , Class<T> clazz) throws Exception{
        Session session = sessionFactory.openSession();
        List<T> list = session.createSQLQuery(sql).addEntity(clazz).list();
        session.close();
        return list;
    }


}
