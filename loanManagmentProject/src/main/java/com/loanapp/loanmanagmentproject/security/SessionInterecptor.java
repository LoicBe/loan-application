package com.loanapp.loanmanagmentproject.security;


import com.loanapp.loanmanagmentproject.appException.SessionException;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionInterecptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);
        if(session == null || session.getAttribute("userID") == null)
            throw  new SessionException("session utilisateur expire veiller se reconnecter ");
        return true;
    }
}
