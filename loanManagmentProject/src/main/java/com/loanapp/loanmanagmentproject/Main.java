package com.loanapp.loanmanagmentproject;

import com.loanapp.loanmanagmentproject.appPack.accountTransaction.Account;
import com.loanapp.loanmanagmentproject.appPack.accountTransaction.AccountDao;

import com.loanapp.loanmanagmentproject.appPack.loanManaging.LoanDao;
import com.loanapp.loanmanagmentproject.appPack.loanManaging.LoanDetailsProvider;
import com.loanapp.loanmanagmentproject.appPack.loanManaging.Loan;
import com.loanapp.loanmanagmentproject.appPack.remboursementManaging.Remboursement;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) throws Exception {

        ApplicationContext app = new ClassPathXmlApplicationContext("conf.xml");
        SessionFactory sessionFactory = app.getBean(SessionFactory.class);
        AccountDao accountDao = app.getBean(AccountDao.class);
        LoanDao loanDao = app.getBean(LoanDao.class);
        System.out.println(sessionFactory);
       // Session session = sessionFactory.openSession();


        /*
        Account account =  new Account();
        account.setId(1);
        account.setUsername("user 2 ");
        account.setOppeningDate(Timestamp.valueOf("2022-02-02 03:05:00"));
        account.setAccountNumber("0001");
        account.setStatus(AccountStatus.ACTIVE);
        account.setExpirationDate(Date.valueOf("2028-09-08"));
        account.setSolde(700.0);
        System.out.println(" befor action ----------------> ");
        account.accountDetails();
        account.retrieve(200.0);
        System.out.println(" after action <---------------- ");
        account.accountDetails();
        account.depot(-900);

         */


      /*  Account account =  new Account();
        account.setUsername("user 1 ");
        account.setSolde(0.0);
        account.setStatus(AccountStatus.ACTIVE);
        account.setOppeningDate(Timestamp.valueOf(LocalDateTime.now()));
        account.setExpirationDate(Date.valueOf("2028-09-09"));
       // session.save(account);
        account.accountDetails();

       */

        Loan loan = loanDao.findByCode("00000004" , "v_loans");
        System.out.println(loan);
        Remboursement[] remboursements = LoanDetailsProvider.buildPayementListe(Date.valueOf("2023-10-10") , loan);
        for(Remboursement remboursement : remboursements){
            System.out.println(remboursement);
        }





    }
}
