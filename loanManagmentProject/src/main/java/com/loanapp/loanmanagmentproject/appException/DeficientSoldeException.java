package com.loanapp.loanmanagmentproject.appException;

public class DeficientSoldeException  extends Exception{
    public DeficientSoldeException(String message) {
        super(message);
    }
}
