package com.loanapp.loanmanagmentproject.appException;


import java.util.List;

public class AuthentificationException  extends  Exception  {

    public AuthentificationException(String message) {
        super(message);
    }




    public static String toStringErrorList (List<String> errors){
        StringBuilder error = new StringBuilder();
        if(errors.size() > 0){
            for(String e : errors){
                error.append(e).append("\n");
            }
        }

        return error.toString();
    }


}
