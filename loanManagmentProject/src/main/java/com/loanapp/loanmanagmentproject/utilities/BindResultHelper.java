package com.loanapp.loanmanagmentproject.utilities;


import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BindResultHelper {


    public static String internaleServerErrorMessage(BindingResult result) throws Exception{
            return BindResultHelper.extractMessage(Objects.requireNonNull(result.getFieldError()));
    }

    private static String extractMessage(FieldError currentError) throws Exception {
        if(!currentError.getDefaultMessage().contains(":")){
            return currentError.getDefaultMessage();
        }
        else
            return currentError.getDefaultMessage().split(":")[1];
    }

    private static List<String>  extractError (BindingResult result){
        List<String> errors = new ArrayList<>();
        for(FieldError fieldError : result.getFieldErrors()){
            errors.add(fieldError.getDefaultMessage());
        }
        return errors;
    }

    public static String toStr(BindingResult result){
        StringBuilder error = new StringBuilder();

        for(String e : BindResultHelper.extractError(result)){

            error.append(e).append("\n");
        }
        return error.toString();
    }


}
