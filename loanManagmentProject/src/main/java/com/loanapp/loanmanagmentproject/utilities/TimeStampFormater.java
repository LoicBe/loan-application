package com.loanapp.loanmanagmentproject.utilities;

import java.sql.Timestamp;

public class TimeStampFormater {



    public static String toValidFormat(String input){
        input = input.replace('T' ,' ');
        input = input+":00";
        return input;
    }

    public static Timestamp transForme(String input){
        return Timestamp.valueOf(TimeStampFormater.toValidFormat(input));
    }

}
