package com.loanapp.loanmanagmentproject.utilities;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;

public class Restore {

    private ArrayList<String> tableName = new ArrayList<String>();

    public ArrayList<String> getTableName() {
        return tableName;
    }

    public void setTableName(ArrayList<String> tableName) {
        this.tableName = tableName;
    }

    public void addTable(String tableName){
        this.getTableName().add(tableName);
    }

    public void cleanDb(Session session)throws Exception{
        Transaction transaction =  null;
        try {
            transaction  = session.beginTransaction();
            for(String table: this.getTableName()){
                String sqlDelete  = "DELETE FROM "+table+" WHERE 1=1";
                session.createSQLQuery(sqlDelete).executeUpdate();
            }
            transaction.commit();

        }
        catch (Exception ex){
            transaction.rollback();
            throw  ex;
        }
    }









}
