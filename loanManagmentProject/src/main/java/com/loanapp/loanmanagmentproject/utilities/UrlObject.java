package com.loanapp.loanmanagmentproject.utilities;

public class UrlObject {

    private String url;
    private String actionName;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public UrlObject(String url, String actionName) {
        this.url = url;
        this.actionName = actionName;
    }
}
