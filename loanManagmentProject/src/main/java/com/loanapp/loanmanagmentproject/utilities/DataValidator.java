package com.loanapp.loanmanagmentproject.utilities;

import java.time.LocalDate;
import java.util.ArrayList;

public class DataValidator {


    public static boolean montantIsValid(String montant) throws Exception{
        Double val = 0.0;
        try{

            val = Double.valueOf(montant);
            if(val<0){
                return false;
            }
            return true;
        }
        catch (Exception e){
            return false;
        }

    }

    public static boolean dateError(int y,int m , int j){
        try{
            LocalDate localDate = LocalDate.of(y,m,j);
            return true;

        }
        catch (Exception e){
            return false;
        }
    }

    public static String toErrorString(ArrayList<String> teste){
        String val = "";
        for(String i : teste){
            val=val+teste+"\n";
        }
        return val;
    }

}
