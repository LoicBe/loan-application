package com.loanapp.loanmanagmentproject.utilities;


import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListHelper {


        private String viewName;
        private UrlObject urlPage;


        private ModelAndView modelAndView;

    public UrlObject getSideBar() {
        return sideBar;
    }

    public void setSideBar(UrlObject sideBar) {
        this.sideBar = sideBar;
    }

        private UrlObject sideBar;
        private String title;

        private HashMap paginationData;
        private List<UrlObject> urls = new ArrayList<>();


    public UrlObject getUrlPage() {
        return urlPage;
    }

    public void setUrlPage(UrlObject urlPage) {
        this.urlPage = urlPage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public HashMap getPaginationData() {
        return paginationData;
    }

    public void setPaginationData(HashMap paginationData) {
        this.paginationData = paginationData;
    }

    public List<UrlObject> getUrls() {
        return urls;
    }

    public void setUrls(List<UrlObject> urls) {
        this.urls = urls;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }


    public void addToUrlAction(UrlObject urlObject){
        this.getUrls().add(urlObject);
    }


    public ListHelper(String viewName, UrlObject urlPage, UrlObject sideBar, String title, HashMap paginationData, List<UrlObject> urls) {
        this.viewName = viewName;
        this.urlPage = urlPage;
        this.sideBar = sideBar;
        this.title = title;
        this.paginationData = paginationData;
        this.urls = urls;
    }

    public ModelAndView renderList(){
        ModelAndView modelAndView = new ModelAndView(this.getViewName());
        modelAndView.addObject("data" , this.getPaginationData().get("data"));
        modelAndView.addObject("nPage", this.getPaginationData().get("nbrePage"));
        modelAndView.addObject("title" , this.getTitle());
        modelAndView.addObject("urls" , this.getUrls());
        modelAndView.addObject("url" ,  this.getUrlPage());
        modelAndView.addObject("sideBar" , this.getSideBar());
        return modelAndView;
    }

    public ListHelper( HashMap paginationData, List<UrlObject> urls) {
        this.paginationData = paginationData;
        this.urls = urls;
    }

    public ListHelper(HashMap paginationData) {
        this.paginationData = paginationData;
    }


}
