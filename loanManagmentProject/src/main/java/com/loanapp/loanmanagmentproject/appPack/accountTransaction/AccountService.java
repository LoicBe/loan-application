package com.loanapp.loanmanagmentproject.appPack.accountTransaction;



import com.loanapp.loanmanagmentproject.appPack.loanManaging.LoanRequest;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class AccountService {



  private final AccountDao accountDao ;

  @Autowired
  public AccountService(AccountDao accountDao) {
      this.accountDao = accountDao;
  }


    private void getActionByTransactionType(Account account , double amount ,  TransactionType transactionType) throws Exception {
        if(transactionType ==TransactionType.IN)
            account.depot(amount);
        if(transactionType==TransactionType.OUT)
            account.retrieve(amount);
    }


    /**
     * transation methode in or out
     *
     *
     * @param account_number
     * @param amount
     * @param actionDate
     * @param session
     * @param transactionType
     * @return
     * @throws Exception
     */
    private  Account transaction_engine(String account_number , double amount ,Timestamp actionDate ,Session session , TransactionType transactionType) throws Exception{

           // transaction  =  session.beginTransaction();
            Account account =  accountDao.findByAccountNumber(account_number);
            getActionByTransactionType(account,amount,transactionType);
            AccountTransaction accountTransaction =  new AccountTransaction(account,amount,actionDate,transactionType);
            session.saveOrUpdate(account);
            session.save(accountTransaction);
            return account;


    }
    public Account retrieve(TransactionRequest transactionRequest) throws Exception {
        Session session =  accountDao.getSessionFactory().openSession();
        Account account =  transaction_engine(transactionRequest.getAccountNumber() , transactionRequest.getAmount() , transactionRequest.getActionDate() , session , TransactionType.OUT);
        session.close();
        return  account;
    }

    public Account depot(TransactionRequest transactionRequest) throws Exception {
        Session session =  accountDao.getSessionFactory().openSession();
        Account account =  transaction_engine(transactionRequest.getAccountNumber() , transactionRequest.getAmount() , transactionRequest.getActionDate() , session , TransactionType.IN);
        session.close();
        return  account;
    }

    public Account transaction(TransactionRequest transactionRequest , TransactionType transactionType , Session session) throws Exception{
       return   transaction_engine(transactionRequest.getAccountNumber() , transactionRequest.getAmount() , transactionRequest.getActionDate() , session , transactionType );
    }

}
