package com.loanapp.loanmanagmentproject.appPack.loanManaging;

import com.loanapp.loanmanagmentproject.appException.ResourceNotFoundException;
import com.loanapp.loanmanagmentproject.dao.GenericDao;
import com.loanapp.loanmanagmentproject.utilities.ListHelper;
import org.springframework.stereotype.Repository;

@Repository
public class LoanDao extends GenericDao {


    public Loan findByCode(String loanCode , String viewName) throws Exception {
        Loan loan = fecthUniqueResult(Loan.class , "SELECT * FROM "+viewName + " where code='"+loanCode+"'");
        if(loan ==null)
            throw  new ResourceNotFoundException("loan not found");
        return  loan;
    }


    public  ListHelper paginateLoanData(int numPage , int limit , String viewName) throws  Exception {
        return new ListHelper( pagination1(
                Loan.class ,
                "SELECT count(*) FROM "+viewName ,
                "SELECT * FROM "+viewName ,
                numPage ,
                limit
        ));
    }



}
