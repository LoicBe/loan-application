package com.loanapp.loanmanagmentproject.appPack.remboursementManaging;

import com.loanapp.loanmanagmentproject.appPack.loanManaging.Loan;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "remboursement_tbl")
public class Remboursement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(updatable = false , insertable = false)
    private String code;
    @ManyToOne
    @JoinColumn(name = "loan_id")
    private Loan loan;
    private Integer tranche;
    @Column(name = "date_echeance")
    private Date echeanceDate;
    @Column(name = "date_payement")
    private Date payDate;
    @Column(name = "late_count")
    private Integer lateCount;
    @Column(name = "late_value")
    private Double lateValue;
    @Column(name = "amount_topay")
    private Double amountToPay;
    private Double interet;
    private Double taux;
    @Column(name = "amount_totale")
    private Double amountTotale;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public Integer getTranche() {
        return tranche;
    }

    public void setTranche(Integer tranche) {
        this.tranche = tranche;
    }

    public Date getEcheanceDate() {
        return echeanceDate;
    }

    public void setEcheanceDate(Date echeanceDate) {
        this.echeanceDate = echeanceDate;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public Integer getLateCount() {
        return lateCount;
    }

    public void setLateCount(Integer lateCount) {
        this.lateCount = lateCount;
    }

    public Double getLateValue() {
        return lateValue;
    }

    public void setLateValue(Double lateValue) {
        this.lateValue = lateValue;
    }

    public Double getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(Double amountToPay) {
        this.amountToPay = amountToPay;
    }

    public Double getInteret() {
        return interet;
    }

    public void setInteret(Double interet) {
        this.interet = interet;
    }

    public Double getTaux() {
        return taux;
    }

    public void setTaux(Double taux) {
        this.taux = taux;
    }

    public Double getAmountTotale() {
        return amountTotale;
    }

    public void setAmountTotale(Double amountTotale) {
        this.amountTotale = amountTotale;
    }

    public Remboursement() {
    }

    public Remboursement(Loan loan, Integer tranche, Date echeanceDate, Date payDate, Integer lateCount, Double lateValue, Double amountToPay, Double interet, Double taux, Double amountTotale) {
        this.loan = loan;
        this.tranche = tranche;
        this.echeanceDate = echeanceDate;
        this.payDate = payDate;
        this.lateCount = lateCount;
        this.lateValue = lateValue;
        this.amountToPay = amountToPay;
        this.interet = interet;
        this.taux = taux;
        this.amountTotale = amountTotale;
    }

    @Override
    public String toString() {
        return "Remboursement{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", loan=" + loan +
                ", tranche=" + tranche +
                ", echeanceDate=" + echeanceDate +
                ", payDate=" + payDate +
                ", lateCount=" + lateCount +
                ", lateValue=" + lateValue +
                ", amountToPay=" + amountToPay +
                ", interet=" + interet +
                ", taux=" + taux +
                ", amountTotale=" + amountTotale +
                '}';
    }
}
