package com.loanapp.loanmanagmentproject.appPack.accountTransaction;

import com.loanapp.loanmanagmentproject.appException.ResourceNotFoundException;

import com.loanapp.loanmanagmentproject.dao.GenericDao;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDao extends GenericDao {



    public Account findByAccountNumber(String account_number) throws Exception {
        Account account =  this.fecthUniqueResult(Account.class , "SELECT * FROM Account WHERE account_number = '"+account_number+"'");
        if(account==null)
            throw new ResourceNotFoundException(" account not found");
        return  account;
    }

}
