package com.loanapp.loanmanagmentproject.appPack.loanManaging;


import com.loanapp.loanmanagmentproject.appPack.accountTransaction.*;
import com.loanapp.loanmanagmentproject.appPack.remboursementManaging.Remboursement;
import com.loanapp.loanmanagmentproject.appPack.remboursementManaging.RemboursementDao;
import com.loanapp.loanmanagmentproject.utilities.ListHelper;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class LoanService {


    private final AccountDao accountDao ;
    private final LoanDao loanDao ;


    private final RemboursementDao remboursementDao;
    private final AccountService accountService;

    @Autowired
    public LoanService(AccountDao accountDao, LoanDao loanDao, RemboursementDao remboursementDao, AccountService accountService) {
        this.accountDao = accountDao;
        this.loanDao = loanDao;
        this.remboursementDao = remboursementDao;
        this.accountService = accountService;
    }

    public Loan save(LoanRequest loanRequest) throws Exception {
        Transaction transaction =  null;
        Session session = null ;
        try {
            session =  accountDao.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Account account = accountDao.findByAccountNumber(loanRequest.getAccountNumber());
            if (account.isValidAccount()) {
                LoanDetailsProvider loanDetailsProvider = loanRequest.getProviderDetails(2.5);
                Loan loan = loanDetailsProvider.buildLoan(account, loanRequest.getDateToTimestamp());
                TransactionRequest transactionRequest =  loan.buildTransactionRequest();
                System.out.println(loan);
                session.save(loan);
                session.save(accountService.transaction(transactionRequest , TransactionType.IN , session));
                transaction.commit();
                return  loan;
            } else
                throw new Exception("account invalid to this action");
        } catch (Exception e) {
            assert transaction != null;
            transaction.rollback();
            e.printStackTrace();
            throw e;

        }
        finally {
            assert session != null;
            session.close();
        }


    }

    public ListHelper waitingList(int num) throws Exception {
        return loanDao.paginateLoanData(num , 20 , "v_waitingLoan");
    }

    public Loan refusedLoan(String loanCode) throws Exception {
        Loan loan =  loanDao.findByCode(loanCode ,  "v_waitingloan");
        loan.refused();
        loanDao.create(loan);
        return  loan;
    }

    public Loan validate(String loanCode , Date datePay , Date  dateValidation) throws Exception {
        Loan loan =  loanDao.findByCode(loanCode ,  "v_waitingloan");
        Session session =  null;
        Transaction transaction = null;
        try {
            session =  loanDao.getSessionFactory().openSession();
            transaction =  session.beginTransaction();
            loan.setValidationDate(dateValidation);
            loan.setDeboursementDate(datePay);
            Remboursement[] loanRemboursementTab = loan.buildPayList(datePay);
            loan.validate();
            session.saveOrUpdate(loan);
            remboursementDao.saveRembList(loanRemboursementTab , session);
            transaction.commit();
            return  loan;
        }
        catch (Exception e){
            assert transaction != null;
            transaction.rollback();
            //e.printStackTrace();
            throw e;
        }
        finally {
            assert session != null;
            session.close();
        }


    }

}
