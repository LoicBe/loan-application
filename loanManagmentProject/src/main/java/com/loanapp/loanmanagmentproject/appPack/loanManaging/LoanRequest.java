package com.loanapp.loanmanagmentproject.appPack.loanManaging;

import com.loanapp.loanmanagmentproject.utilities.TimeStampFormater;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

public class LoanRequest {

    @NotBlank(message = "account number cant'be null")
    @Pattern(message = "accountNumber invalid" , regexp = "^[0-9]+$"  , flags = Pattern.Flag.CASE_INSENSITIVE)
    private String accountNumber  ;
    @NotBlank(message = "date requierd")
    private String date;
    @Min(value = 0  , message = "amount can't be negative")
    private Double amount;
    @Min(value = 1 , message = "duration invalide")
    private Integer duration;


    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }


    public  LoanDetailsProvider getProviderDetails( double tauxInteret){
        LoanDetailsProvider loanDetailsProvider =  new LoanDetailsProvider();
        loanDetailsProvider.setAmount(this.getAmount());
        loanDetailsProvider.setDuration(this.getDuration());
        loanDetailsProvider.setTauxInteret(tauxInteret);
        return  loanDetailsProvider;
    }


    public Timestamp getDateToTimestamp(){
        return TimeStampFormater.transForme(this.date);
    }




    @Override
    public String toString() {
        return "LoanRequest{" +
                "accountNumber='" + accountNumber + '\'' +
                ", date='" + date + '\'' +
                ", amount=" + amount +
                ", duration=" + duration +
                '}';
    }
}
