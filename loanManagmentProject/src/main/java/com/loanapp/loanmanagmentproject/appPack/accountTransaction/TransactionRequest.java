package com.loanapp.loanmanagmentproject.appPack.accountTransaction;

import com.loanapp.loanmanagmentproject.utilities.TimeStampFormater;
import org.hibernate.Transaction;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

public class TransactionRequest {

    private TransactionType transactionType ;
    @NotBlank(message = "account number requiered")
    @Pattern(message = "accountNumber invalid" , regexp = "^[0-9]+$"  , flags = Pattern.Flag.CASE_INSENSITIVE)
    private String accountNumber ;
    @Min(value =   0 ,  message = "amount cant'be negative")
    private double amount;
    private String dateAction;

    private Timestamp tmestampValue;

    public Timestamp getTmestampValue() {
        return tmestampValue;
    }

    public void setTmestampValue(Timestamp tmestampValue) {
        this.tmestampValue = tmestampValue;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDateAction() {
        return dateAction;
    }

    public Timestamp getActionDate(){
        if(this.getDateAction()!=null)
            return TimeStampFormater.transForme(this.getDateAction());
        if(this.getTmestampValue()!=null)
            return  this.getTmestampValue();
        return null;
    }

    public void setDateAction(String dateAction) {
        this.dateAction = dateAction;
    }


    public void in(){
        this.transactionType =  TransactionType.IN ;
    }
    public void out(){
        this.transactionType = TransactionType.OUT ;
    }


    public TransactionRequest() {
    }

    public TransactionRequest(String accountNumber, double amount, Timestamp tmestampValue) {
        this.accountNumber = accountNumber;
        this.amount = amount;
        this.tmestampValue = tmestampValue;
    }

    @Override
    public String toString() {
        return "TransactionRequest{" +
                "transactionType=" + transactionType +
                ", accountNumber='" + accountNumber + '\'' +
                ", amount=" + amount +
                ", dateAction=" + dateAction +
                " ,timestamp="+getActionDate()+
                '}';
    }
}
