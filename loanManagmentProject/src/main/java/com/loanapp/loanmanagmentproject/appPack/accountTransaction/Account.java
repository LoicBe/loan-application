package com.loanapp.loanmanagmentproject.appPack.accountTransaction;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;

/**
 * user account representation
 */

@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "account_number" , insertable = false , updatable = false)
    private String accountNumber;
    private String username;
    private Double solde;
    @Column(name = "opening_date")
    private Timestamp oppeningDate;
    @Enumerated(EnumType.STRING)
    private AccountStatus status ;
    @Column(name = "date_expiration")
    private Date expirationDate ;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public Timestamp getOppeningDate() {
        return oppeningDate;
    }

    public void setOppeningDate(Timestamp oppeningDate) {
        this.oppeningDate = oppeningDate;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    private boolean isExpired(){
        return this.expirationDate.before(Date.valueOf(LocalDate.now()));
    }

    private boolean isAdecouvert(){
        return this.getSolde()==0;
    }




    /**
     * check account validity
     * verify status
     * verify expiration
     * @return boolean
     */
    public boolean isValidAccount() throws Exception {
        if(this.status != AccountStatus.ACTIVE)
            throw  new Exception("account banned or desactived");
        if(this.isExpired())
            throw  new Exception("account validity expired");
        return this.status == AccountStatus.ACTIVE && !this.isExpired();
    }


    private void verifyIsNegativeAmount(double amount) throws IllegalArgumentException {
        if(amount<0)
            throw  new IllegalArgumentException("amount invalid to transaction : "+amount);
    }

    /**
     * account retrait methode
     *
     * @param amount
     * @throws Exception
     */

    public void retrieve(double amount) throws Exception {
        verifyIsNegativeAmount(amount);
        if((isValidAccount())){
           if(!isAdecouvert() && this.solde >= amount)      //solde checking operation
                this.solde -= amount;
           else
               throw new IllegalArgumentException("insfuffisant solde for tis action : "+amount+" current solde : "+solde);
        }
        else
            throw  new Exception("error during execute request");
    }


    /**
     * account depot methode
     * @param amount
     * @throws Exception
     */
    public void depot(double amount) throws Exception {
        verifyIsNegativeAmount(amount);
        if((isValidAccount())){
            this.solde += amount;
        }
        else
            throw  new Exception("error during execute request");
    }


    /**
     * desactivation account methode
     */
    public void desactiveAccount(){
        if(this.status != AccountStatus.DESACTIVED)
            this.status =  AccountStatus.DESACTIVED;
    }

    /**
     *  activation account methode
     */
    public void activateAccount(){
        if(this.status != AccountStatus.ACTIVE)
            this.status =  AccountStatus.ACTIVE;
    }


    // for teste
    public void accountDetails(){
        System.out.println("-------------------------------------");
        System.out.println("acount_number : "+this.accountNumber);
        System.out.println("username : "+this.username);
        System.out.println("opening_date : "+this.oppeningDate);
        System.out.println("expirationdate : "+this.expirationDate);
        System.out.println("status : "+this.status);
        System.out.println(" **** solde : "+this.solde);
    }





}
