package com.loanapp.loanmanagmentproject.appPack.loanManaging;

import com.loanapp.loanmanagmentproject.appPack.accountTransaction.Account;
import com.loanapp.loanmanagmentproject.appPack.loanManaging.Loan;
import com.loanapp.loanmanagmentproject.appPack.loanManaging.LoanRequest;
import com.loanapp.loanmanagmentproject.appPack.loanManaging.LoanStatus;
import com.loanapp.loanmanagmentproject.appPack.remboursementManaging.Remboursement;
import net.bytebuddy.pool.TypePool;

import java.sql.Array;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

public class LoanDetailsProvider {

    private Double amount;
    private Integer duration;
    private Double tauxInteret;


    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Double getTauxInteret() {
        return tauxInteret;
    }

    public void setTauxInteret(Double tauxInteret) {
        this.tauxInteret = tauxInteret;
    }





    private static double interetSimpleCalculator(double amount , double taux , int periode){
        return amount * taux/100.0 * periode;
    }

    private  double noCumulationInteretCalculator(){
        return interetSimpleCalculator(this.amount , this.tauxInteret , this.duration);
    }

    private double cumulationInteretCalculator(){
        double interetTotale = 0.0;
        double constValue = this.amount /(double)this.duration ;
        double valueToGetCumulation = amount;
        boolean cumulable = true ;
        while ( cumulable ){
            cumulable = valueToGetCumulation > 0;
            double interet  = interetSimpleCalculator(valueToGetCumulation  , this.tauxInteret , 1);
            valueToGetCumulation -= constValue ;
            interetTotale += interet ;
        }
        return  interetTotale ;
    }


    /**
     * build an loan by the details provide int this class
     *
     * @param account
     * @param date
     * @return
     * @throws Exception
     */

    public Loan buildLoan(Account account , Timestamp date) throws Exception {
        double interetVal = ineteretCalculator(true) ;
        Loan loan =  new Loan();
        loan.setAmount(this.amount);
        loan.setDuration(this.duration);
        loan.setStatus(LoanStatus.WAITING);
        loan.setTauxInteret(this.tauxInteret);
        loan.setIneteret(interetVal);
        loan.setBeneficiaireAccount(account);
        loan.setTotaleValue(amount+interetVal);
        loan.setDate(date);
        return  loan;
    }

    public  double ineteretCalculator(boolean cumulation){
        if(cumulation)
            return  cumulationInteretCalculator();
        else
            return  noCumulationInteretCalculator();
    }


    // TODO: 25/09/2023
    /*
    cheking date end of the payement
     */
    public static Remboursement[] buildPayementListe(Date beginPay , Loan loan) {
        double amountToPay = loan.getAmount();
        ArrayList<Remboursement> remboursements = new ArrayList<>();
        for(int i = 1 ; i<=loan.getDuration() ;  i++){
            double  loanMensualite = loan.getMensualite();
            double interet = LoanDetailsProvider.interetSimpleCalculator(amountToPay , loan.getTauxInteret() , 1);
            double totaleToPay = loanMensualite + interet ;
            remboursements.add(
                    new Remboursement(loan ,i ,beginPay ,null,0,0.0,amountToPay ,interet ,loan.getTauxInteret()  ,totaleToPay)
            );
            amountToPay -=loanMensualite;
            beginPay  = Date.valueOf(beginPay.toLocalDate().plusMonths(1));
        }
        return remboursements.toArray(new Remboursement[0]);
    }







    @Override
    public String toString() {
        return "LoanDetailsProvider{" +
                "amount=" + amount +
                ", duration=" + duration +
                ", tauxInteret=" + tauxInteret +
                '}';
    }
}
