package com.loanapp.loanmanagmentproject.appPack.loanManaging;


import com.loanapp.loanmanagmentproject.utilities.BindResultHelper;
import com.loanapp.loanmanagmentproject.utilities.ListHelper;
import com.loanapp.loanmanagmentproject.utilities.UrlObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.sql.Date;
import java.time.LocalDate;

@Controller
@RequestMapping("/loanApp/loanApi")
public class LoanController {

    private final LoanService loanService;

    @Autowired
    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    @GetMapping("/newLoan")
    public ModelAndView newLoan(){
        return new ModelAndView("loan/createLoan");
    }

    @PostMapping("/createLoan")
    public ModelAndView createLoan(@Valid @ModelAttribute LoanRequest loanRequest , BindingResult result) throws Exception {
        if(!result.hasErrors()){
            try{
                loanService.save(loanRequest);
                return  newLoan();
            }catch (Exception e){
                e.printStackTrace();
                return newLoan().addObject("error" , e.getMessage());
            }
        }
        else
            return newLoan().addObject("error" , BindResultHelper.toStr(result));

    }

    @GetMapping("/waiting")
    public ModelAndView waitingListe(@RequestParam(name = "num" , defaultValue = "1") int num) throws Exception {
        ListHelper listHelper =  loanService.waitingList(num);
        listHelper.setTitle("waiting loan");
        listHelper.setViewName("loan/loanListe");
        listHelper.setUrlPage(new UrlObject("/loanApp/loanApi/waiting" , ""));
        return listHelper.renderList();
    }



    @GetMapping("/refuse/{code}")
    public ModelAndView refused(@PathVariable(name = "code") String loanCode) throws Exception {
        try {
            loanService.refusedLoan(loanCode);
            return waitingListe(1);
        }
        catch (Exception e){
            e.printStackTrace();
            return  waitingListe(1).addObject("error" , e.getMessage());
        }
    }

    @GetMapping("/validate/{code}")
    public ModelAndView validate(@PathVariable(name = "code") String loanCode ) throws Exception {
        try {
            loanService.validate(loanCode , Date.valueOf("2023-11-09") , Date.valueOf("2023-09-25"));
            return waitingListe(1);
        }
        catch (Exception e){
            e.printStackTrace();
            return  waitingListe(1).addObject("error" , e.getMessage());
        }
    }



}
