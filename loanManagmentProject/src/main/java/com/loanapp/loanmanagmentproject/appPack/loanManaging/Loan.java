package com.loanapp.loanmanagmentproject.appPack.loanManaging;

import com.loanapp.loanmanagmentproject.appPack.accountTransaction.Account;
import com.loanapp.loanmanagmentproject.appPack.accountTransaction.TransactionRequest;
import com.loanapp.loanmanagmentproject.appPack.remboursementManaging.Remboursement;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "loanaction_tbl")
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(insertable = false)
    private String code;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account beneficiaireAccount;
    @Column(name = "loan_date")
    private Timestamp date;
    @Column(name = "loan_amount")
    private Double amount;
    @Column(name = "loan_duration")
    private Integer duration;
    @Column(name = "taux_interet")
    private Double tauxInteret ;
    @Column(name = "loans_interet")
    private Double ineteret ;
    @Column(name ="totale_value")
    private Double totaleValue;
    @Column(name = "validation_date")
    private Date validationDate;
    @Column(name = "deboursement_date")
    private Date deboursementDate;
    @Column(name = "echeance_date")
    private Date echeance;
    @Enumerated(EnumType.STRING)
    @Column(name = "loan_status")
    private LoanStatus status ;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Account getBeneficiaireAccount() {
        return beneficiaireAccount;
    }

    public void setBeneficiaireAccount(Account beneficiaireAccount) {
        this.beneficiaireAccount = beneficiaireAccount;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Double getTauxInteret() {
        return tauxInteret;
    }

    public void setTauxInteret(Double tauxInteret) {
        this.tauxInteret = tauxInteret;
    }

    public Double getIneteret() {
        return ineteret;
    }

    public void setIneteret(Double ineteret) {
        this.ineteret = ineteret;
    }

    public Date getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(Date validationDate) {
        this.validationDate = validationDate;
    }

    public Date getDeboursementDate() {
        return deboursementDate;
    }

    public void setDeboursementDate(Date deboursementDate) {
        this.deboursementDate = deboursementDate;
    }

    public Date getEcheance() {
        return echeance;
    }

    public void setEcheance(Date echeance) {
        this.echeance = echeance;
    }

    public LoanStatus getStatus() {
        return status;
    }

    public void setStatus(LoanStatus status) {
        this.status = status;
    }

    public Double getTotaleValue() {
        return totaleValue;
    }

    public void setTotaleValue(Double totaleValue) {
        this.totaleValue = totaleValue;
    }


    public Double getMensualite(){
        return this.amount/(double )this.getDuration();
    }


    public void refused() throws Exception {
        if(this.status == LoanStatus.WAITING)
            this.status = LoanStatus.REFUSED;
        else
            throw new Exception("this loan is already refused or validate");
    }


    public void validate() throws Exception {
        if(this.status == LoanStatus.WAITING){
            this.status = LoanStatus.VALIDATE;
            this.echeance = Date.valueOf( this.deboursementDate.toLocalDate().plusMonths(this.getDuration()) );
        }

        else
            throw new Exception(" this loan cant'be validate");
    }


    public Remboursement[] buildPayList(Date datePay) throws Exception {
        if(this.getStatus() ==LoanStatus.WAITING){
            return LoanDetailsProvider.buildPayementListe(datePay , this);
        }
        else
            throw new Exception(" status error , cant't generate pay list");
    }








    TransactionRequest buildTransactionRequest(){
        return new TransactionRequest(this.beneficiaireAccount.getAccountNumber(), this.getAmount() , this.getDate());
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", beneficiaireAccount=" + beneficiaireAccount +
                ", date=" + date +
                ", amount=" + amount +
                ", duration=" + duration +
                ", tauxInteret=" + tauxInteret +
                ", ineteret=" + ineteret +
                ", totaleValue=" + totaleValue +
                ", validationDate=" + validationDate +
                ", deboursementDate=" + deboursementDate +
                ", echeance=" + echeance +
                ", status=" + status +
                '}';
    }
}
