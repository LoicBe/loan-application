package com.loanapp.loanmanagmentproject.appPack.loanManaging;

public enum LoanStatus {

    WAITING ,
    VALIDATE ,
    REFUSED

}
