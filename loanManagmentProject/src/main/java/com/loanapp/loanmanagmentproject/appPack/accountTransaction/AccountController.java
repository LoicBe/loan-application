package com.loanapp.loanmanagmentproject.appPack.accountTransaction;



import com.loanapp.loanmanagmentproject.utilities.BindResultHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;

@Controller
@RequestMapping("/loanApp/accountApi")
public class AccountController {


    private final AccountService accountService ;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/newAction")
    public String newAccountAction(){
        return "account/actionAccount";
    }

    @PostMapping("/depot")
    public String newAccountActionSubmit(@Valid  @ModelAttribute TransactionRequest request , BindingResult result) throws Exception{
        if(!result.hasErrors()){
            //request.in();
            System.out.println(request);
            try{
                accountService.depot(request);
                return  newAccountAction();
            }
            catch (Exception e){
                e.printStackTrace();
                return "redirect:/loanApp/accountApi/newAction?error="+ e.getMessage();
            }
        }
        else
            return "redirect:/loanApp/accountApi/newAction?error="+ BindResultHelper.toStr(result);

    }
    @PostMapping("/retrait")
    public String newAccountActionSubmit1(@Valid  @ModelAttribute TransactionRequest request , BindingResult result) throws Exception{
        if(!result.hasErrors()){
            System.out.println(request);
            try{
                accountService.retrieve(request);
                return  newAccountAction();
            }
            catch (Exception e){
                e.printStackTrace();
                return "redirect:/loanApp/accountApi/newAction?error="+ e.getMessage();
            }
        }
        else
            return "redirect:/loanApp/accountApi/newAction?error="+ BindResultHelper.toStr(result);

    }






}
