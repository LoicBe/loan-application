package com.loanapp.loanmanagmentproject.appPack.accountTransaction;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "transaction_tbl")
public class AccountTransaction {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "code" , updatable = false , insertable = false)
    private String transactionCode ;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account ;
    private Double amount ;
    @Column(name = "transaction_date")
    private Timestamp transactionDate;
    @Enumerated(EnumType.STRING)
    @Column(name = "trsc_type")
    private TransactionType transactionType ;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Timestamp getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Timestamp transactionDate) {
        this.transactionDate = transactionDate;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public AccountTransaction() {
    }

    public AccountTransaction(Account account, Double amount, Timestamp transactionDate, TransactionType transactionType) {
        this.account = account;
        this.transactionDate = transactionDate;
        this.transactionType = transactionType;
        if(transactionType== TransactionType.OUT)
            this.amount = amount * -1.0;
        if(transactionType ==TransactionType.IN)
            this.amount =  amount;

    }
}
