package com.loanapp.loanmanagmentproject.appPack.remboursementManaging;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class RemboursementDao {


    public void saveRembList(Remboursement[] remboursements , Session session) throws Exception{
        for(Remboursement remboursement : remboursements){
            session.save(remboursement);
        }
    }


}
